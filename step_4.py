from step_3 import go_to_line

def file_navigate_with_statment(file_name):
    with open(file_name,mode='r') as f:
        lines = f.readlines()
        file_lines = len(lines)
        visits = {}
        line_num = 0
        while True: 
            line=lines[line_num].split(' ')
            match line[0]:
                case "goto":
                    line_num=go_to_line(line)

                    if visits.get(line_num) or line_num >= file_lines:
                        break
                    else :
                        visits[line_num] = "visited"
                case "remove":
                    try:
                        lines.pop(int(line[1]))
                        file_lines -=1
                        line_num=int(line[1]) 
                    except:
                        line_num+=1
                case "replace":
                    try:
                        lines[int(line[1])] = line[2]
                        line_num+=1
                    except:
                        pi+=1
                case _:
                    print(f"Something wrong {line}")
        print(line_num+1)

# file_navigate_with_statment('.\step_4.txt')


