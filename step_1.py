def basic_calc(op,num_a,num_b):
    match op:
        case "+":
            return int(num_a) + int(num_b) 
        case "-":
            return int(num_a) - int(num_b)
        case "x":
            return int(num_a) * int(num_b)
        case "/": 
            return  f"Div with zero is not allowed" if int(num_b) == 0 else int(num_a) / int(num_b)
        case "^":
            return int(num_a) ** int(num_b)
        case "%":
            return int(num_a) % int(num_b)
        case _ :
            return f"Operration {op} is not implemented"


#Another change

# Change one

# basic_calc('/','1','2')
# basic_calc('-','1','2')
# basic_calc('+','1','2')
# basic_calc('/','1','0')
# basic_calc('*','1','2')
# basic_calc('(','1','2')