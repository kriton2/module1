from math import floor
from step_1 import basic_calc

def go_to_line(line):
    if line[1] == "calc":
        return floor(basic_calc(line[2],line[3],line[4])) - 1
    else:
        return int(line[1]) - 1

def file_navigate(file_name):
    with open(file_name,mode='r') as f:
        lines = f.readlines()
        file_lines = len(lines)
        visits = {}
        line_num = 0
        while True:
            line=lines[line_num].split(' ')
            match line[0]:
                case "goto":
                    line_num=go_to_line(line)

                    if visits.get(line_num) or line_num >= file_lines:
                        break
                    else :
                        visits[line_num] = "visited"
                    print(f"goto line {line_num + 1} this is the original {line}")
                case _:
                    print('Something wrong')
        print(line_num+1)

# file_navigate('.\step_3.txt')