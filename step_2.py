from step_1 import  basic_calc

def calc_sum(file_name):
    s=0
    with open(file_name,mode='r') as f:
        for line in f.readlines():
            line = line.strip().split(' ')
            try:
                s+=basic_calc(line[1],line[2],line[3])
            except:
                continue #poor error handling
    return s

# calc_sum('.\step_2.txt')